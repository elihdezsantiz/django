from django.forms import modelform_factory
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from webapp.models import Datos


def bienvenido(request):
    ##return HttpResponse("<h1 style=\"color:blue;\">Hola Mundo</h1>")
    no_datos = Datos.objects.count()
    datos = Datos.objects.all()
    ##mensajes={
       ## "msg1": "usuario1"
    ##}
    ##return render(request,"Pagina1/hola.html", mensajes,{'no_datos':no_datos})
    return render(request, "Pagina1/hola.html", {'no_datos': no_datos, 'datos':datos})

def mostrar_datos(request,id):
    dato = Datos.objects.get(pk=id)
    return render(request,'Pagina1/detalle.html',{'dato':dato})


DatosForm = modelform_factory(Datos, exclude= [])
def datosNuevo(request):

    formaDatos=DatosForm()
    return render(request,'Pagina1/nuevo.html',{'formaDatos':formaDatos})